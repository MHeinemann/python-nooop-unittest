import unittest

import my_programm


class TestUtils(unittest.TestCase):
    def test_is_prime(self):
        self.assertFalse(my_programm.is_prime(4))
        self.assertTrue(my_programm.is_prime(2))
        self.assertTrue(my_programm.is_prime(3))
        self.assertFalse(my_programm.is_prime(8))
        self.assertFalse(my_programm.is_prime(10))
        self.assertTrue(my_programm.is_prime(7))
        self.assertEqual(my_programm.is_prime(-3),
                         "Negative numbers are not allowed")

    def test_cubic(self):
        self.assertEqual(my_programm.cubic(2), 8)
        self.assertEqual(my_programm.cubic(-2), -8)
        self.assertNotEqual(my_programm.cubic(2), 4)
        self.assertNotEqual(my_programm.cubic(-3), 27)

    def test_say_hello(self):
        self.assertEqual(my_programm.say_hello("Geekflare"), "Hello, Geekflare")
        self.assertEqual(my_programm.say_hello("Chandan"), "Hello, Chandan")
        self.assertNotEqual(my_programm.say_hello("Chandan"), "Hi, Chandan")
        self.assertNotEqual(my_programm.say_hello("Hafeez"), "Hi, Hafeez")


if __name__ == '__main__':
    unittest.main()
